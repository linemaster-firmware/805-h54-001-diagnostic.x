/*******************************************************************************
LSC No.:		905-R42
By:				JM
Description:	Firmware for Hemodia wired analog/RS232 output
Target:			PIC18F25K20
PCBA:			905-X41

History:
Version:			001
04-18-2019		Created
 Version:           002
 06-17-2019
 Changes requested on May 22 2019 email from Dias Armando
 

*******************************************************************************/
#define		DEBUG_MODE          1				// uncomment for debug
//#define         ENABLE_BOR          1


#include    "LSC_805_H54_Config.h"
#include    "../Librarys/PCBAs/805-H54/805-H54-Working.X/805_H54_PCBA.h"
#include    "../Librarys/PCBAs/805-H54/805-H54-Working.X/805_H54_EEprom.h"
#include    "../Librarys/PCBAs/805-H54/805-H54-Working.X/805_H54_A2D.h"
#include    "../Librarys/PCBAs/805-H54/805-H54-Working.X/805_H54_MCP4728.h"
#include    "../Librarys/PCBAs/805-H54/805-H54-Working.X/805_H54_TMR.h"
#include    "../Librarys/PCBAs/805-H54/805-H54-Working.X/805_H54_CRC_8.h"

#include 	"805-H54-001-test.h"

	// Set Configuration Bits
#ifdef		DEBUG_MODE
#pragma 	config  		DEBUG = ON
#pragma		config			WDTEN = OFF
#pragma		config			CPB = OFF
#pragma		config			CP0 = OFF
#pragma		config			CP1 = OFF
#pragma		config			CP2 = OFF
#pragma		config			CP3 = OFF
#else
#pragma 	config  		DEBUG = OFF
#pragma		config			WDTEN = OFF
#pragma		config			CPB = ON
#pragma		config			CP0 = ON
#pragma		config			CP1 = ON
#pragma		config			CP2 = ON
#pragma		config			CP3 = ON
#endif	

#pragma		config			CPD = OFF
#pragma		config			FOSC = INTIO67
#pragma		config			FCMEN = OFF
#pragma		config			IESO = OFF
#pragma		config			PWRT = ON
#ifdef      ENABLE_BOR
    #pragma		config			BOREN = SBORDIS
#else
    #pragma		config			BOREN = OFF
#endif
#pragma		config			BORV = 27
#pragma 	config  		WDTPS = 256			// 1.024 Seconds
#pragma 	config			LPT1OSC = ON
#pragma 	config  		HFOFST = OFF
#pragma 	config  		MCLRE = OFF
#pragma 	config  		STVREN = OFF
#pragma 	config  		XINST = OFF
#pragma 	config  		LVP = OFF
#pragma		config			PBADEN = OFF

#pragma romdata SW_VER
rom const unsigned char SW_VER[] = {'9','0','5','-','R','4','2',' ',' ','V','e','r',':','0','0','2'};

// Hi ISR Vector
#pragma code InterruptVectorHigh = 0x0008
void InterruptVectorHigh (void)
{
  _asm
    goto InterruptHandlerHigh 
  _endasm
}

// Lo ISR Vector
#pragma code InterruptVectorLow = 0x0018
void InterruptVectorLow (void)
{
  _asm
    goto InterruptHandlerLow 
  _endasm
}

// debug //
 rom char test_data[] = {0x50, 0x43, 0x42, 0x2D, 0x54, 0x45, 0x53, 0x54};


#pragma code main = 0x002a
 void main (void){
	static unsigned int x;
    
    
	while (!CLOCK_STABLE)	
		ClrWdt();

#ifdef	DEBUG_MODE
	DISABLE_WDT;
#else
	ENABLE_WDT;
#endif
    
	RCON = 0xff;
	for (FSR0 = (unsigned char) 0x0000; FSR0 < _MAX_RAM; FSR0++){
		INDF0 = 0x00;
	}
    
	Init_IO ();
    Init_SFR ();
    VCP_Init();
    
#if (USE_PLL == TRUE)
    #define		SYS_FREQ				(float)64.0e6
    //#define		TIMER0_PRESCALER		(float)16    
    OSCTUNEbits.PLLEN = 1;
    Init_TMR_3 (TMR3_ISR_HI, TMR3_PRE_8, TMR3_INT_CLK);
    Init_TMR_0 (TMR0_ISR_LOW, TMR0_PRE_256, TMR0_16_BIT);
#else
    #define		SYS_FREQ				(float)16.0e6
    OSCTUNEbits.PLLEN = 0;
    //#define		TIMER0_PRESCALER		(float)4
    Init_TMR_0 (TMR0_ISR_LOW, TMR0_PRE_4, TMR0_16_BIT);  
#endif    
   
	ENABLE_PRIORITY_INT;	
	ENABLE_INT_HI;
	ENABLE_INT_LO;
    TIMER_0_ON;
 

	ClrWdt();	
	Nop();

	if (Virgin){							// First Ever Run

        
	}		

    Start_tx = 0;
 
	while (1){					// infinite program loop same as RF
		ClrWdt ();

        // If set check CRC and initiate the transmission
        if(Rx_vcp_byte_flag)
        {
            Rx_vcp_byte_flag = 0;
            if (Rx_vcp_Buffer[7] == Calc_CRC_8(Rx_vcp_Buffer, Rx_VCP_Packet_Size - 1))
            {
                // Note: Tx handler placed temporarily to test for transmission
                // Tx handler will be placed in Rx_handle_commands after implementation 
                VCP_Tx_Handler(test_data, 8);
                Tx_vcp_byte_flag = 0;
                Nop();
            }
        }

    }
}	
 
/*******************************************************************************
Function:		VCP_Init
By:				AB
Description:	Initializes the data direction registers associated with Tx & Rx 
                pins, clears the INT on change in PORTB.4 pin flag, and 
                initializes the flags associated with Rx & Tx.
Passed:			Not a thing
Returns:        void
History:
03-02-2021		Created
*******************************************************************************/
 void VCP_Init(void)
 {
    unsigned char scratch;
    
    // VCP Pins Data direction registers initializations
    Tx_PIN_DIRECTION = OUTPUT;
    Rx_PIN_DIRECTION = INPUT;
    DISBALE_WEAK_PULLUP_P4;
    
    // To clear the Interrupt on change in PORTB.4 flag bit
    ENABLE_IOCB_P4;
    scratch = PORTB;
    CLEAR_RX_INT_FLAG;
    ENABLE_VCP_INT;
    VCP_Rx_HIGH_IP;

    // TX flags initializations 
    Tx_vcp_bit_counter = 0;
    Tx_vcp_byte_counter = 0;
    Tx_vcp_byte_flag = 0;
    Start_tx = 0;
    
    // Tx data buffer initializations 
    Tx_data.Byte = 0x00;
    VCP_Tx_ptr = 0;
    
    // Rx flags initializations
    Rx_vcp_bit_counter = 0;
    Rx_vcp_byte_flag = 0;
    Rx_vcp_packet_timeout = 0;
    Start_rx = 0;
    
    // Rx data buffer initializations
    Rx_data.Byte = 0x00;
    VCP_Rx_ptr = 0;
    
    
 }
 
/*******************************************************************************
Function:		VCP_Tx_Handler
By:				AB
Description:	Transmits data in response to PLT command. Starts transmitting 
                if Start_tx flag is set and stops after transmitting required
                number of bytes. Transmits byte by byte and initializes the TMR3H 
                & TMRL registers before start sending a byte.
           
Passed:			cmd_resp    - Response string to PL command
                no_of_bytes - No of bytes in the string to be sent as response
Returns:        void
History:
03-02-2021		Created
*******************************************************************************/
 void VCP_Tx_Handler(rom char* cmd_resp, unsigned char no_of_bytes)
 {
    // To check for NULL pointer if passed inadvertently 
    if(cmd_resp == 0)
    {
        return;
    }
    
    while(no_of_bytes != 0){
        if(!Start_tx){
            
            // To load timer registers with ticks to interrupt every 104us and initialize Tx flags
            VCP_Tx_Init();
            
            // To set Tx data buffer byte by byte to command response data
            Tx_data.Byte = *cmd_resp++;
            
            // To send start bit Tx pin PORTC.6 is set to zero
            Tx_VCP = 0;  
            no_of_bytes--;
        }
    }
    
    // Flag set after sending required number of bytes
    Tx_vcp_byte_flag = 1;
}
 
/*******************************************************************************
Function:		Init_SFR
By:				JM
Description:	Initializes the Special Function Registers
Passed:			Not a thing
Returns:		Same
History:
06-30-2010		Created
*******************************************************************************/
void Init_SFR (void){
	SSPMSK = 0x00;
	// ANSEL (analog Select default to all off)
	ANSEL = 0x00;
	ANSELH = 0x00;
    
#if	(AN1_Enable)    
    ANSELbits.ANS0 = 1;
    Analog_In[0].Flags.Enable = 1;
#endif
#if	(AN2_Enable)
    ANSELbits.ANS1 = 1;
    Analog_In[1].Flags.Enable = 1;
#endif
#if	(AN3_Enable)    
    ANSELbits.ANS2 = 1;
    Analog_In[2].Flags.Enable = 1;
#endif
    // Vin Analog
	ANSELbits.ANS4 = 1;

	// PIE1
                            PIE1bits.ADIE = 0;
#if RS232_ENABLED == TRUE
    PIE1bits.RCIE = 1;
#else
    PIE1bits.RCIE = 0;
#endif 
	PIE1bits.TXIE = 0;
	PIE1bits.SSPIE = 0;
	PIE1bits.CCP1IE = 0;

	// PIE2
	PIE2bits.OSCFIE = 0;
	PIE2bits.C1IE = 0;
	PIE2bits.C2IE = 0;
	PIE2bits.EEIE = 0;
	PIE2bits.BCLIE = 0;
	PIE2bits.HLVDIE = 0;
	PIE2bits.CCP2IE = 0;
	// IPR1

	IPR1bits.CCP1IP = 0;
	IPR1bits.SSPIP = 0;
	IPR1bits.TXIP = 0;
	IPR1bits.RCIP = 0;	// UART priority
                            IPR1bits.ADIP = 0;
	// IPR2
	IPR2bits.OSCFIP = 0;
	IPR2bits.C1IP = 0;
	IPR2bits.C2IP = 0;
	IPR2bits.EEIP = 0;
	IPR2bits.BCLIP = 0;
	IPR2bits.HLVDIP = 0;

	IPR2bits.CCP2IP = 0;

#if (USE_PLL == TRUE)
	OSCTUNEbits.PLLEN = 1;    
#else
	OSCTUNEbits.PLLEN = 0;    
#endif

	// PWM1CON
	PWM1CON = 0x00;
	// CCP2CON
	CCP2CON = 0x00;
	// ADCON2
    ADCON2bits.ADCS0 = 0;
    ADCON2bits.ADCS1 = 1;
    ADCON2bits.ADCS2 = 1;
    ADCON2bits.ACQT0 = 0;
    ADCON2bits.ACQT1 = 1;
    ADCON2bits.ACQT2 = 0;
    ADCON2bits.ADFM = 1;

	// INTCON
	INTCONbits.GIE = 0;
	INTCONbits.PEIE = 0;
	INTCONbits.INT0IE = 0;

    // INTCON2
	INTCON2bits.RBPU = 0;
	INTCON2bits.INTEDG0 = 1;
	INTCON2bits.INTEDG1 = 1;
	INTCON2bits.INTEDG2 = 1;
	
    
	//INTCON3
	INTCON3bits.INT2IP = 0;
	INTCON3bits.INT1IP = 0;
	INTCON3bits.INT2IE = 0;
	INTCON3bits.INT1IE = 0;
	//OSCCON
	OSCCONbits.IDLEN = 0;		// enter Idle mode during Sleep
	OSCCONbits.IRCF2 = 1;		// default high frequency, 000 for 31khz
	OSCCONbits.IRCF1 = 1;		// 110 8Mb, 111 16Mb
	OSCCONbits.IRCF0 = 1;
    
   
#if (USE_PLL == TRUE)
	OSCCONbits.SCS1 = 0;
	OSCCONbits.SCS0 =0;
	OSCTUNEbits.PLLEN = 1;     
#else
	OSCCONbits.SCS1 = 1;
	OSCCONbits.SCS0 =0;
	OSCTUNEbits.PLLEN = 0;     
#endif   

	return;
}

/*******************************************************************************
Function:		VCP_Tx_Init
By:				AB
Description:	Initializes the TMR3H & TMR3L registers with tick value, switch 
                on the TIMER3, initializes Tx start, and Tx bit counter flags
Passed:			Not a thing
Returns:        void
History:
03-02-2021		Created
*******************************************************************************/
void VCP_Tx_Init()
{
    BitWise_16	Wake_Period;
    unsigned char scratch;
 
    // Load TIMER3 with ticks to interrupt at 104us to start the transmission
    Wake_Period.Word = ~TICKS_PER_BIT;
    TMR3H = Wake_Period.Byte[1];
    TMR3L = Wake_Period.Byte[0]; 
    scratch = TMR3L;

    Start_tx = 1;
    
    // To start the TIMER3 and start counting the ticks
    TIMER_3_IF = 0;
    TIMER_3_INT_ON;
    TIMER_3_ON;

    Tx_vcp_bit_counter = 0;
}

  

/*******************************************************************************
Function:		InterruptHandlerHigh
By:				AB
Description:	Handles High level interrupts on change on PROTB.4 and TIMER3 
                interrupts to transmit & receive data
Passed:			Not a thing
Returns:		void
History:
03-02-2021		Created
*******************************************************************************/
#pragma interrupt InterruptHandlerHigh 
void  InterruptHandlerHigh (void){
	BitWise_16	Wake_Period;
    unsigned char scratch;
    
    // Interrupt on change in PORTB.4
    if (VCP_PB4_INT_FLAG & VCP_PB4_ENABLE){
        
        // To avoid recurrent interrupts, clear the interrupt flag & disable the PORTB.4 change interrupt
        scratch = PORTB;
        CLEAR_RX_INT_FLAG;
        DISABLE_VCP_INT;      
        
        // To set the sampling point on receiving data, set the TIMER3 with ticks to interrupt at 156us after start bit
        Wake_Period.Word = ~TICKS_SAMPLING_INTERVAL;
        TMR3H = Wake_Period.Byte[1];
        TMR3L = Wake_Period.Byte[0];
        
        // To start the TIMER3 and start counting the ticks
        TIMER_3_IF = 0;   
        TIMER_3_ON;
        TIMER_3_INT_ON;
      
        // To start reception and initialize packet timeout to 15ms
        Start_rx = 1;
        if (VCP_Rx_ptr == 0){
            SW_Timer[VCP_Packet_Timeout] = TICKS_PER_PACKET_TIMEOUT;
        }
    }

    // TIMER3 interrupt to receive and transmit data 
    if(TIMER_3_IF & TIMER_3_IE){
 
        // Reload TIMER3 with ticks to interrupt at every 104us 
		Wake_Period.Word = ~TICKS_PER_BIT;
        TMR3H = Wake_Period.Byte[1];
        TMR3L = Wake_Period.Byte[0]; 
        scratch = TMR3L;
        
        TIMER_3_IF = 0;   

        // TX portion
        if(Start_tx == 1)
        {
            // Transmit 8 data bits (1 byte)
            if(Tx_vcp_bit_counter < 0x08) 
            {   
                Tx_VCP = Tx_data.b0;
                Tx_data.Byte >>= 1;    
                Tx_vcp_bit_counter++;
            }
            // Transmit 2 stop bits
            else if(Tx_vcp_bit_counter < 0x0A)
            {
                Tx_VCP = 1; //send stop bit
                Tx_vcp_bit_counter++;
            }
            // Stop the TIMER3 to stop the transmission 
            else
            {
                TIMER_3_OFF; // making sure timer 3 is on before sending stop bit out
                TIMER_3_INT_OFF;
                Tx_data.Byte = 0x00;
                Start_tx = 0;
                Tx_vcp_bit_counter = 0;
            }
        }
        
        // Rx Portion
        else if(Start_rx)
        { 
            // Receive 8 data bits (1 byte) 
            if(Rx_vcp_bit_counter < 0x08)
            {                
                Rx_data.Byte >>= 1;
                Rx_data.b7 = Rx_VCP;
                TMR3H = Wake_Period.Byte[1];
                TMR3L = Wake_Period.Byte[0];      
                Rx_vcp_bit_counter++;
            }
            // Check if a byte is received 
            else if(Rx_vcp_bit_counter == 0x08)
            {   
                // Check for stop bit 
                if (Rx_VCP == 1)
                {
                    Rx_vcp_Buffer[VCP_Rx_ptr++] = Rx_data.Byte;
                   
                    // Check if a 8 byte packet is received 
                    if(VCP_Rx_ptr == Rx_VCP_Packet_Size)
                    {
                        // Set byte flag if a 8 byte packet is received
                        VCP_Rx_ptr = 0;
                        Rx_vcp_byte_flag = 1;    
                    }
                }
                
                // Clear the PORTB.4 INT flag and enable Interrupt on change in PORTB.4
                scratch = PORTB;
                CLEAR_RX_INT_FLAG;
                ENABLE_VCP_INT;
                
                // Stop the TIMER3 to stop sampling data before interrupt on change in PORTB.4 fire
                Rx_vcp_bit_counter = 0;
                Start_rx = 0;
                TIMER_3_OFF;
                TIMER_3_INT_OFF;   
            }
        }
    }

	return;
}

/*******************************************************************************
Function:		InterruptHandlerLow
By:				AB
Description:	Handles Low level TIMER0 interrupts on every 1ms           
Passed:			Not a thing
Returns:		void
History:
03-02-2021		Created
*******************************************************************************/
#pragma interruptlow InterruptHandlerLow
void InterruptHandlerLow (void){

    unsigned char x;
	BitWise_16 Wake_Period;
    
    // Timer 0 Interrupt
    if (TIMER_0_IF & TIMER_0_IE){	

        // Clear interrupt flag 
		TIMER_0_IF = 0;  
        
        // Reload the TIMER0 registers to interrupt every 1ms
        Wake_Period.Word = ~TICKS_VCP_TIMER;
		TMR0H = Wake_Period.Byte[1];
		TMR0L = Wake_Period.Byte[0]; 
        
        // Decrement packet time out ticks by 1 for every 1ms time-out  
        for(x = 0; x < NUM_OF_TIMERS; x++)
        {
            if(SW_Timer[x])
            {
                SW_Timer[x]--;
            }
        }
        
        // To check packet time out failure i.e failed to receive a 8 byte packet
        if(!SW_Timer[VCP_Packet_Timeout] && VCP_Rx_ptr != 0)
        {
            Nop();
            VCP_Rx_ptr = 0;
            Rx_vcp_packet_timeout = 1;
        }
    }
    
 	return;
}


