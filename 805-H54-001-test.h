/*******************************************************************************
By:				AB
Description:	header file for 805-C48-001-test.c 
History:
03/02/21		Created 
*******************************************************************************/

/* Function Prototypes */
void InterruptHandlerLow (void);
void InterruptHandlerHigh (void);
void Init_SFR (void);

/* Virtual COM Port Init function prototype */
void VCP_Init(void);

/* Virtual COM Port Transmitter function prototypes*/
void VCP_Tx_Init(void);
void VCP_Tx_Handler(rom char* cmd_resp, unsigned char no_of_bytes);

unsigned char Calc_CRC_8 (unsigned char *buf, unsigned char length);


/* Virtual COM port Baud Rate definitions */
#define     VCP_BAUD_RATE                            9600
#define     VCP_BIT_RATE              (float)        (1.0 / (float)VCP_BAUD_RATE)

/* Virtual COM port clock frequency definition*/
//#define		SYS_FREQ				  (float)        64.0e6

/* VCP TIMER3 Configurations for sampling the data */
#define		INTERUPT_OVERHEAD		  (float)        4.0e-6		
#define		TIMER3_PRESCALER		  (float)        8.0
#define     TIMER3_RESOLUTION		  (float)        ((float)(4/SYS_FREQ)*TIMER3_PRESCALER)
#define     VCP_SAMPLING              (float)        ((float)VCP_BIT_RATE + ((float)VCP_BIT_RATE  / 2.0))
#define     TICKS_SAMPLING_INTERVAL   (unsigned int) ((float) (VCP_SAMPLING - INTERUPT_OVERHEAD) / (float)TIMER3_RESOLUTION)
#define     TICKS_PER_BIT             (unsigned int) ((float)(VCP_BIT_RATE - INTERUPT_OVERHEAD) / (float)TIMER3_RESOLUTION)
//#define     TICKS_PER_BIT             (unsigned int) ((float)VCP_BIT_RATE  / (float)TIMER3_RESOLUTION)

/* VCP general purpose TIMER0 Configurations */
#define     TIMER0_PRESCALER		  (float)         256.0
#define		TIMER0_RESOLUTION		  (float)        ((float)(4/SYS_FREQ)*TIMER0_PRESCALER)
#define     VCP_TIMER                 (float)         1.0e-3
#define     TICKS_VCP_TIMER           (unsigned int) ((float)VCP_TIMER /(float)TIMER0_RESOLUTION)

/* VCP Pin Definitions */
#define     Rx_VCP                    PORTBbits.RB4
#define     Tx_VCP                    LATCbits.LATC6

/* VCP Rx registers configurations */
#define     Rx_PIN_DIRECTION          TRISBbits.TRISB4 
#define     VCP_PB4_ENABLE            INTCONbits.RBIE
#define     ENABLE_VCP_INT            INTCONbits.RBIE = 1
#define     DISABLE_VCP_INT           INTCONbits.RBIE = 0 
#define     VCP_Rx_HIGH_IP            INTCON2bits.RBIP = 1
#define     VCP_Rx_LOW_IP             INTCON2bits.RBIP = 0
#define     VCP_PB4_INT_FLAG          INTCONbits.RBIF
#define     CLEAR_RX_INT_FLAG         INTCONbits.RBIF = 0
#define     ENABLE_IOCB_P4            IOCBbits.IOCB4 = 1 
#define     DISABLE_IOCB_P4           IOCBbits.IOCB4 = 0 
#define     DISBALE_WEAK_PULLUP_P4    WPUBbits.WPUB4 = 0

/* VCP Tx registers configurations */
#define     Tx_PIN_DIRECTION          TRISCbits.TRISC6 

/* VCP Receiver Packet Definitions */
#define		Rx_VCP_Packet_Size                       8
#define     RX_VCP_PACKET_TIMEOUT     (float)        15.0e-3
#define     TICKS_PER_PACKET_TIMEOUT  (unsigned int)(RX_VCP_PACKET_TIMEOUT / TIMER0_RESOLUTION)	

/* VCP Receiver buffer variables */
BitWise_8                             Rx_data;
unsigned char                         Rx_vcp_Buffer[Rx_VCP_Packet_Size];
unsigned char                         VCP_Rx_ptr;

/* VCP Receiver flag variables */
unsigned char                         Rx_vcp_bit_counter;
unsigned char                         Rx_vcp_byte_flag;
unsigned char                         Rx_vcp_packet_timeout;
unsigned char                         Start_rx;

/* VCP Transmitter buffer variables */
BitWise_8                             Tx_data;
unsigned char                         VCP_Tx_ptr;

/* VCP Transmitter flag variables */
unsigned char                         Tx_vcp_bit_counter;
unsigned char                         Tx_vcp_byte_counter;
unsigned char                         Tx_vcp_byte_flag;
unsigned char                         Start_tx;

/* SW timers */
enum _SW_Timers{
		Delay_Timer = 0,
        System_Update,
        Button_Hold,
        Bit_2_Dwell,
        VCP_Packet_Timeout,
        Idle_Update_Timer,
        NUM_OF_TIMERS
};
unsigned int	SW_Timer[NUM_OF_TIMERS] = {0};




